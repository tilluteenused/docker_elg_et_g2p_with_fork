# Eesti kirjakeelse sõna häälduspäraseks teisendamise reeglistikku sisaldav Docker'i konteiner

[Kirjakeelse sõna häälduspäraseks teisendamise reeglistikku](https://gitlab.com/tilluteenused/docker_elg_et_g2p_with_fork.git) sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Kirjakeelse sõna häälduspäraseks teisendamise reeglistik](https://github.com/tv4om/fork_et_g2p.git)
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara (juhised on [git'i veebilehel](https://git-scm.com/)) ja
Java JDK (testitud **_openjdk-11-jdk_**'ga)
<!--
```commandline
sudo apt -y install openjdk-11-jdk
sudo update-alternatives --config java
java -version
sudo apt -y install ant 
```
-->

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/et_g2p_fork:2022.08.11
```

 Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

<!----
Lähtekood koosneb 2 osast
1. json liides, veebiserver ja konteineri tegemise asjad
2. Taneli asjad
----->

Lähtekood tuleb laadida alla kahest kohast.

#### 1.1 [Konteineri ja liidesega seotud lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_et_g2p_with_fork.git) allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_et_g2p gitlab_docker_elg_et_g2p_with_fork
```

#### 1.2 [Kirjakeelse sõna häälduspäraseks teisendamise reeglistiku](https://github.com/tv4om/fork_et_g2p.git) lähtekoodi allalaadimine

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_et_g2p_with_fork
git clone https://github.com/tv4om/fork_et_g2p.git github_fork_et_g2p
```

### 2. Lähtekoodi kompileerimine ja testimine

**NB!** Arvutis peab olema installitud ja vaikimisi töötama _openjdk-11-jdk_ !!!

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_et_g2p_with_fork/github_fork_et_g2p
ant
```

### 3. Konteineri kokkupanemine 

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_et_g2p_with_fork
docker build -t tilluteenused/et_g2p_fork:2022.08.11 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/et_g2p_fork:2022.08.11
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* tühikuga eraldatud sõnede loend */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [             /* töödeldud sõnede massiiv */
      {
        "content": string, /* sõne */
        "features":
        {
          "warning": string,              /* optional, mingi probleem */
          "pronunciations": [string, ...] /* optional, häälduspärased kujud */
        }
      }
    ]
  }
}
```
## Kasutusnäide

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"OECD ABC-pood paµk palgi_koorem"}' localhost:5000/process  localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.2.2 Python/3.10.4
Date: Thu, 11 Aug 2022 13:21:47 GMT
Content-Type: application/json
Content-Length: 420
Connection: close

{"response":{"type":"texts","texts":[{"content":"OECD","features":{"pronunciations":["o e k t","o o e e t s e e t e e"]}},{"content":"ABC-pood","features":{"pronunciations":["a p k p o o t","a a p e e t s e e p o o t"]}},{"content":"pa\u00b5k","features":{"warning":"cannot convert word, reason:Unknown character [\u00b5] in input"}},{"content":"palgi_koorem","features":{"pronunciations":["p a l k i k o o r e m"]}}]}}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
